package com.opulenceinteractive.codingchallenge.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.opulenceinteractive.codingchallenge.Model.APIItem
import com.opulenceinteractive.codingchallenge.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.api_item_recycler_row.view.*

class ItemAdapter (private val itemList : ArrayList<APIItem>, private val listener : Listener) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    interface Listener {

        fun onItemClick(apiItem : APIItem)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(itemList[position], listener, position)
    }

    override fun getItemCount(): Int = itemList.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.api_item_recycler_row, parent, false)

        return ViewHolder(view)
    }

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {

        fun bind(apiItem: APIItem, listener: Listener, position: Int) {

            Picasso.get().load(apiItem.imageURL).into(itemView.api_item_image_view)
            itemView.api_item_title_text.text = String.format("Title: %s", apiItem.title)
            apiItem.author?.let{author ->
                itemView.api_item_author_text.text = String.format("Author: %s", author)
            }
            itemView.setOnClickListener{ listener.onItemClick(apiItem) }
        }
    }
}