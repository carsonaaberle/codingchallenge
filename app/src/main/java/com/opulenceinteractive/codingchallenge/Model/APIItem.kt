package com.opulenceinteractive.codingchallenge.Model

data class APIItem(val title: String, val imageURL: String, val author: String?)