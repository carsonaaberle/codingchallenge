package com.opulenceinteractive.codingchallenge

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import com.opulenceinteractive.codingchallenge.Adapters.ItemAdapter
import com.opulenceinteractive.codingchallenge.Model.APIItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import android.content.Intent
import android.net.Uri
import java.net.URLEncoder


class MainActivity : AppCompatActivity(), ItemAdapter.Listener {

    private var compositeDisposable: CompositeDisposable? = null
    private var mAdapter: ItemAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val requestInterface = Retrofit.Builder()
            .baseUrl("http://de-coding-test.s3.amazonaws.com/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create())
            .build().create(APIRequestInterface::class.java)

        compositeDisposable = CompositeDisposable()

        setUpRecyclerView()

        compositeDisposable?.add(requestInterface.getBooksAndMovies()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::handleResponse, this::handleError))

    }

    private fun setUpRecyclerView() {

        main_recycler_view.setHasFixedSize(true)
        val layoutManager : RecyclerView.LayoutManager = LinearLayoutManager(this)
        main_recycler_view.layoutManager = layoutManager
    }

    private fun handleResponse(apiItemList: List<APIItem>) {

        Log.d("TAG", apiItemList.toString())
        mAdapter = ItemAdapter(apiItemList as ArrayList<APIItem>, this)

        main_recycler_view.adapter = mAdapter
    }

    private fun handleError(error: Throwable) {
        //Normally I do some more robust error handling like display, error codes, and error reporting.

        Toast.makeText(this, "Error ${error.localizedMessage}", Toast.LENGTH_SHORT).show()
    }

    override fun onItemClick(apiItem: APIItem) {
        val query = URLEncoder.encode(apiItem.title, "utf-8")
        val url = "http://www.google.com/search?q=$query"
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable?.clear()
    }
}
