package com.opulenceinteractive.codingchallenge

import com.opulenceinteractive.codingchallenge.Model.APIItem
import io.reactivex.Observable
import retrofit2.http.GET

interface APIRequestInterface {
    @GET("/books.json")
    fun getBooksAndMovies(): Observable<List<APIItem>>
}
