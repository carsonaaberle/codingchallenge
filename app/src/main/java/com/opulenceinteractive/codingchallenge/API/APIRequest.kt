import com.opulenceinteractive.codingchallenge.Model.APIItem
import io.reactivex.Observable
import retrofit2.http.GET

interface APIRequest {

    @GET("/posts")
    fun getBooksAndMovies(): Observable<List<APIItem>>
}